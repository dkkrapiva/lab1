﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace firstp
{
    class Notebook
    {
        public static List<Note> list = new List<Note>();
        static void Main(string[] args)
        {
            int N = -1;
            while (N != 0)
            {
                Console.WriteLine("Запись нового элемента - 1");
                Console.WriteLine("Редактировать запись - 2");
                Console.WriteLine("Удалить запись - 3");
                Console.WriteLine("Просмотреть записи - 4");
                Console.WriteLine("Просмотр всех созданных учетных записей, с краткой информацией - 5");
                Console.WriteLine("Выход - 0");
                N = int.Parse(Console.ReadLine());
                if (N == 1)
                {
                    string SurN = Console.ReadLine();
                    string name = Console.ReadLine();
                    string country = Console.ReadLine();
                    ulong phoneNumber = ulong.Parse(Console.ReadLine());
                    Note note = new Note(name, SurN, country, phoneNumber);
                    list.Add(note);
                }
                if (N == 2)
                {
                    editNote();
                }
                if (N == 3)
                {
                    deleteNote();
                }
                if (N == 4)
                {
                    showNote();
                }
                if (N == 5)
                {
                    showAllNotes();
                }
            }
        }
        private static int getId()
        {
            int id;
            Console.WriteLine("Введите id:");
            id = int.Parse(Console.ReadLine());
            return id;
        }
        private static string getString(string a)
        {
            Console.Write($"{a}: ");
            string input;
            while ((input = Console.ReadLine()) == "")
            {
                Console.Write($"Введите {a}: ");
            }
            return input;
        }
        private static ulong getPhoneNumber(ulong phoneNumber = 0)
        {
            if (phoneNumber == 0)
            {
                Console.Write("Номер телефона: ");
            }
            else
            {
                Console.Write($"Номер телефона ({phoneNumber}): ");
            }

            while (!ulong.TryParse(Console.ReadLine(), out phoneNumber))
            {
                Console.Write("Номер введен некорректно, введите номер: ");
            }
            return phoneNumber;
        }
        private static void showNote()
        {

            int id = getId();
            Note ids = list.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }
            Console.WriteLine(ids.ToString());
        }
        private static void editNote()
        {

            int id = getId();
            Note ids = list.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }

            ids.surname = getString($"Фамилия ({ids.surname})");

            ids.name = getString($"Имя ({ids.name})");

            Console.Write($"Отчество ({ids.middleName}): ");
            ids.middleName = Console.ReadLine();

            ids.phoneNumber = getPhoneNumber(ids.phoneNumber);

            ids.country = getString($"Страна ({ids.country})");

            Console.Write($"Дата рождения ({ids.birthdate}): ");
            ids.birthdate = Console.ReadLine();
            Console.Write($"Организация ({ids.work}): ");
            ids.work = Console.ReadLine();
            Console.Write($"Должность ({ids.position}): ");
            ids.position = Console.ReadLine();
            Console.Write($"Прочие заметки ({ids.other}): ");
            ids.other = Console.ReadLine();
        }
        private static void deleteNote()
        {

            int id = getId();
            Note ids = list.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }
            list.Remove(ids);
        }
        private static void showAllNotes()
        {
            Console.WriteLine("Записи:");
            if (list.Count == 0)
            {
                Console.WriteLine("Нет записей");
            }
            foreach (var note in list)
            {
                Console.WriteLine($"Id: {note.id}\nФамилия: {note.surname}\nИмя: {note.name}\nНомер телефона: {note.phoneNumber}");
                Console.WriteLine("---------------------------------");
            }
        }
    }
    public class Note
    {
        public string name;
        public string surname;
        public string middleName;
        public ulong phoneNumber;
        public string country;
        public string birthdate;
        public string work;
        public string position;
        public string other;
        public int id;
        private static int idcounter = 0;
        public Note(string name, string surname, string country, ulong phoneNumber, string middleName = null, string birthdate = null, string work = null, string position = null, string other = null)
        {
            this.name = name;
            this.surname = surname;
            this.country = country;
            this.phoneNumber = phoneNumber;
            this.middleName = middleName;
            this.birthdate = birthdate;
            this.work = work;
            this.position = position;
            this.other = other;
            this.id = idcounter++;
        }
        public override string ToString()
        {
            return $"Имя: {name}\nФамилия: {surname}\nОтчество: {middleName}\n" +
                $"Номер телефона: {phoneNumber}\nСтрана: {country}\nДата рождения: {birthdate}\nОрганизация: {work}\n" +
                $"Должность: {position}\nПрочие заметки: {other}";
        }
    }
}
